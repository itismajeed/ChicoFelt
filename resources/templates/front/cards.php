
<div class="row m-5 text-center justify-content-center" id="cards">
    <?php
    if(isset($_GET['page'])){
        $p=$_GET['page'];
    }else{
        $p=1;
    }
    ?>
    <?php foreach (getProducts($p) as $key => $value): ?>
    <div class="card col-lg-3 col-md-5  m-3 p-0">
        <img class="card-img-top"  src="../resources/uploads/<?php echo $value['location_img'] ?>" alt="Card image cap">
        <div class="card-block mt-3 p-3">
            <h4 class="card-title font-weight-bold"><?php echo $value["title"] ?></h4>
            <?php
            $d = new DateTime($value['created_ts']);
            $date = jdate("j F o  G:i", $d->getTimestamp());
            ?>
            <small class="text-muted" dir="rtl"><?php echo $date?></small>
            <p class="card-text text-center  text-justify" dir="rtl"><?php echo truncate($value['description'],15); ?></p>
            <?php if(isOrdered(currentUser(),$value['id'])): ?>
            <p class="text-muted">خریده شده</p>
            <?php endif; ?>
            <hr>
                <div class="float-left">
                <div  data-rateyo-rating="<?php echo ceil(getRate($value['id'])['ave']) ?>" class="rateYo float-left" data-rateyo-read-only="true"></div>
                <span class=" text-warning" style="font-size: .8rem;">(<?php echo getRate($value['id'])['cnt'] ?>)</span>
                </div>

            <p class="card-text float-right text-muted " dir="rtl" ><span class="text-success"><?php echo number_format($value["price"])?></span>تومان</p>
            <div class="clearfix"></div>
            <a class="btn btn-block btn-primary mt-1 text-white" href="item.php?id=<?php echo $value['id'] ?>" style="cursor: pointer">جزئیات</a>
        </div>
    </div>
    <?php endforeach; ?>

</div>

