<?php require_once "../../resources/functions.php" ?>
<?php if (isAdmin()) { ?>
    <!doctype html>

    <html lang="fa-IR" class="h-100">
    <meta charset=" UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/jquery.rateyo.min.css">
    <link rel="stylesheet" href="css/style.css">

    <title>پنل مدیریت</title>
    </head>
    <body class="h-100">

    <?php include_once TMP_FRONT . DS . 'mainHeader.php' ?>
    <div class="row h-100 m-0 text-white" style="padding-top: 3.7rem" id="app">

        <?php include_once TMP_BACK . DS . 'leftCol.php' ?>
        <?php include_once TMP_BACK . DS . 'users.php' ?>
    </div>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.rateyo.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/vue.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
    <script type="text/javascript" src="js/admin.js"></script>
    </body>
    </html>
<?php } else {
    logMessage("trying to access admin area ip(" . $_SERVER['REMOTE_ADDR'] . ").");
}
?>