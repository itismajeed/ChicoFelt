<div class="card chicoBlue col-sm-3 text-white text-center m-3" style="height: 15rem;">
    <div class="card-block">
        <h4 class="pt-5 pb-3 font-weight-bold" style="border-bottom: 2px solid white; font-family: Shabnam;" dir="rtl" >
            <span>تعداد محصولات : </span>
            <span class="font-weight-bold"><?php echo productsCount()['cnt'] ?></span>
        </h4>
        <br><br>
        <div class="card-text">
            <a href="" class="btn btn-block btn-outline-light" @click.prevent="showNewNamad=!showNewNamad">
                <span class="fa fa-plus"></span>
                <span>اضافه کردن نمد جدید</span>
            </a>
        </div>

    </div>
</div>
<!--card no 1-->

<div class="card chicoBlue col-sm-3 text-white text-center m-3" style="height: 15rem;">
    <div class="card-block">
        <h4 class="pt-5 pb-3 font-weight-bold" style="border-bottom: 2px solid white; font-family: Shabnam;" >
            <span>تعداد کاربران فعلی : </span>
            <span class="font-weight-bold"><?php echo usersCount()['cnt'] ?></span>
        </h4>
        <br><br>
        <div class="card-text">
            <a href="usersManagement.php" class="btn btn-block btn-outline-light">
                <span class="fa fa-users"></span>
                <span>مدیریت کاربران</span>
            </a>
        </div>

    </div>
</div>
<!--card no 2-->

<div class="card chicoBlue col-sm-3 text-white text-center m-3" style="height: 15rem;">
    <div class="card-block">
        <h4 class="pt-5 pb-3 font-weight-bold" style="border-bottom: 2px solid white; font-family: Shabnam;" dir="rtl" >
            <span>تعداد سفارشات کل : </span>
            <span class="font-weight-bold"><?php echo ordersCount()['cnt'] ?></span>
        </h4>
        <br><br>
        <div class="card-text">
            <a href="ordersManagement.php" class="btn btn-block btn-outline-light">
                <span class="fa fa-cart-arrow-down"></span>
                <span>مدیریت سفارشات</span>
            </a>
        </div>

    </div>
</div>
<!--card no 3-->


