<!--Main Header-->
<header>
    <!--Navigation Bar-->
    <nav class="navbar chicoNavbar chicoWhite">
        <div class="container">
            <a class="chicoLogo text-left font-weight-bold text-dark" href="<?php echo HOME_URL ?>" style="font-size: 1.5rem;text-decoration: none; transition:  500ms ease;font-family: Samim;
">
                چیکوفلت
            </a>
            <!--left Links-->
            <?php if (!isLoggedIn()): ?>
                <span>
                <a href="auth.php" class="text-right">ورود</a>
                <a href="auth.php">عضویت</a>
                <a class="" href="<?php echo HOME_URL."cart.php" ?>" dir="rtl">  <span class="fa fa-shopping-cart "></span> <span
                            class="badge badge-pill badge-success">
                        <?php if(isset($_COOKIE['carting'])){if(isset($_SESSION['products'])){echo count($_SESSION["products"]);}else{echo 0;}}else{echo 0;} ?>
                    </span></a>
                </span>
                <!--Right Links-->
            <?php endif; ?>
            <?php if (isLoggedIn()): ?>
                <span dir="rtl">
                  <a class="" href="<?php echo HOME_URL."cart.php" ?>" dir="rtl">
                    <span class="fa fa-shopping-cart "></span> <span
                          class="badge badge-pill badge-success">
                          <?php if(isset($_COOKIE['carting'])){if(isset($_SESSION['products'])){echo count($_SESSION["products"]);}else{echo 0;}}else{echo 0;} ?>
                    </span>
                  </a>
                 <a href="<?php echo HOME_URL ?>"
                    class="text-warning "><span class="fa fa-<?php if (isAdmin()) {
                         echo "address-card";
                     } else {
                         echo "user-circle";
                     } ?>"></span> <span><?php echo $_SESSION['showingName']; ?></span> </a>

                <a href="<?php echo HOME_URL . '?logout=1' ?>" class="text-dark ">  <span
                            class="fa fa-sign-out"></span> <span>خروج</span> </a>

                    <?php if (isAdmin()): ?>
                        <a href="<?php echo ADMIN_URL?>" class="text-dark"><span class="fa fa-pie-chart"></span> پنل مدیریت </a>
                    <?php endif; ?>

                </span>

            <?php endif; ?>
        </div>
        <!--Container-->
    </nav>
    <!--End Navigation Bar-->

</header>
<!--End Main Header-->
<?php

if (isset($_GET['logout'])) {
    logOut();
}
?>